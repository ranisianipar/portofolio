from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class PortofolioUnitTest(TestCase):

	def test_portofolio_url_is_exist(self):
		response = Client().get('/homepage/')
		
		self.assertEqual(response.status_code, 200)

	def test_portofolio_using_index_func(self):
		found = resolve('/homepage/')
		self.assertEqual(found.func, index)