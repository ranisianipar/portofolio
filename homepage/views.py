from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
	response['author'] = 'Rani Lasma Uli'
	return render(request,'index.html',response)